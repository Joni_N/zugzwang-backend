package org.zugzwang.server.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zugzwang.server.dto.PlayerDto;
import org.zugzwang.server.dto.mapper.PlayerMapper;
import org.zugzwang.server.model.Player;
import org.zugzwang.server.model.PlayerList;
import org.zugzwang.server.repository.PlayerListRepository;
import org.zugzwang.server.repository.PlayerRepository;

@Service
public class PlayerService {

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    PlayerListRepository playerListRepository;

    @Autowired 
    PlayerMapper playerMapper;

    public Player getPlayer(String id) throws Exception {
        Optional<Player> player = playerRepository.findById(id);
        return player.orElseThrow(()-> new Exception());
    }

    public PlayerList getPlayerList(String name) throws Exception{
        Optional<PlayerList> playerList = playerListRepository.findByName(name);
        return playerList.orElseThrow(()-> new Exception());
    }

    public Player create(PlayerDto dto) throws Exception {
        Player player = playerMapper.dtoToPlayer(dto);
        return playerRepository.save(player);
    }

	public void delete(String id) {
        playerRepository.deleteById(id);
	}
    
}