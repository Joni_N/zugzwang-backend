package org.zugzwang.server.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zugzwang.server.model.Team;
import org.zugzwang.server.repository.TeamRepository;

@Service
public class TeamService {

    @Autowired
    TeamRepository teamRepository;

    public Team getTeam(Long id) throws Exception {
        Optional<Team> team = teamRepository.findById(id);
        return team.orElseThrow(()-> new Exception());
    }

    public Team addTeam(Team team) throws Exception {
        Team ret = teamRepository.save(team);
        return ret;
    }

    public Iterable<Team> addTeams(Iterable<Team> teams) throws Exception {
        Iterable<Team> ret = teamRepository.saveAll(teams);
        return ret;
    }
    
}
