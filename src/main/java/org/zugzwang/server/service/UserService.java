package org.zugzwang.server.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zugzwang.server.dto.UserDto;
import org.zugzwang.server.dto.mapper.UserMapper;
import org.zugzwang.server.model.User;
import org.zugzwang.server.repository.UserRepository;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserMapper mapper;

    public UserDto getUser(Long id) throws Exception {
        Optional<User> user = userRepository.findById(id);
        UserDto dto = mapper
                .usertoUserDto(user.orElseThrow(() -> new Exception()));
        return dto;
    }
}