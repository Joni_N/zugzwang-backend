package org.zugzwang.server.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.zugzwang.server.model.Player;

@Repository
public interface PlayerRepository extends CrudRepository<Player, String> {
    
}