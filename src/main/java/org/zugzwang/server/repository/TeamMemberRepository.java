package org.zugzwang.server.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.zugzwang.server.model.TeamMember;

@Repository
public interface TeamMemberRepository extends CrudRepository<TeamMember, Long> {
    
}