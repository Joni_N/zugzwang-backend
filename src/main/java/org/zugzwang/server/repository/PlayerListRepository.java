package org.zugzwang.server.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.zugzwang.server.model.PlayerList;

@Repository
public interface PlayerListRepository extends CrudRepository<PlayerList, Long> {
    Optional<PlayerList> findByName(String name);
}