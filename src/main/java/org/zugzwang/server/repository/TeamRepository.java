package org.zugzwang.server.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.zugzwang.server.model.Team;

@Repository
public interface TeamRepository extends CrudRepository<Team, Long> {
    
}