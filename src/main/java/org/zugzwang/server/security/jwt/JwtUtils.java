package org.zugzwang.server.security.jwt;

import java.security.Key;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

@Component
public class JwtUtils {

    @Value("${jwt.expTime}")
    private String validityMins;

    private Key key;
    JwtUtils(@Value("${jwt.secret}") String secretKeyBase64Enc) {
        key = Keys.hmacShaKeyFor(secretKeyBase64Enc.getBytes());
    }

    public String createJwtToken(String username) {
        Date expDate = new Date(System.currentTimeMillis() + Long.valueOf(validityMins) * 60 * 1000);
        String jws = Jwts.builder()
            .setSubject(username)
            .setIssuedAt(new Date())
            .setExpiration(expDate)
            .signWith(key)
            .compact();
        return jws;
    }

    // Returns user name if token is valid
    public String verifyJwtToken(String token) {
        Jws<Claims> jws;
        try {
            jws = Jwts.parserBuilder()   
                .setSigningKey(key)    
                .build()                    
                .parseClaimsJws(token); 
                System.out.println(jws.getBody().getSubject());
            return jws.getBody().getSubject();
        }  
        catch (JwtException ex) {      
            System.out.println(ex.getMessage());
        }
        return null;
    }
}