package org.zugzwang.server.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.zugzwang.server.model.User;
import org.zugzwang.server.repository.UserRepository;

@Service
public class ZZUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;
   
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User " + username + "not found!"));
        
        return ZZUserDetails.build(user);
    }
}