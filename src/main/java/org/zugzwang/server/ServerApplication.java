package org.zugzwang.server;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.zugzwang.server.model.Player;
import org.zugzwang.server.model.PlayerList;
import org.zugzwang.server.model.Role;
import org.zugzwang.server.model.RoleName;
import org.zugzwang.server.model.Team;
import org.zugzwang.server.model.TeamMember;
import org.zugzwang.server.model.User;
import org.zugzwang.server.repository.PlayerListRepository;
import org.zugzwang.server.repository.TeamRepository;
import org.zugzwang.server.repository.UserRepository;
import org.zugzwang.server.security.jwt.JwtUtils;

@SpringBootApplication
public class ServerApplication implements ApplicationRunner {
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	TeamRepository teamRepository;

	@Autowired
	PlayerListRepository playerListRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	JwtUtils utils;

	public static void main(final String[] args) {
		SpringApplication.run(ServerApplication.class, args);
	}

	@Override
	public void run(final ApplicationArguments args) throws Exception {
		User user = new User("user", "joni.nordlund@gmail.com",
				passwordEncoder.encode("password"));
		Set<Role> roles = new HashSet<>();
		Role role = new Role(RoleName.ROLE_USER);
		roles.add(role);
		user.setRoles(roles);

		try {
			userRepository.save(user);

		} catch (Exception e) {
			// TODO: handle exception
		}
		//
		addPlayers();
		System.out.println(LocalDate.now().plusDays(3));
	}

	void addPlayers() {
		System.out.println("CALLING ADD PLAYERS");
		PlayerList plist = new PlayerList("TestList", "Joni");
		Player pl1 = new Player("Joni", "Nordlund", "PSY");
		Player pl2 = new Player("Katja", "Itkonen", "PSY");
		Player pl3 = new Player("Tuula", "Lakonen", "RSY");
		Player pl4 = new Player("Sami", "Ontelo", "Garde");
		Player pl5 = new Player("Jussi", "Haikala", "TammerShakki");
		Player pl6 = new Player("Tero", "Lassio", "PSY");
		plist.addPlayer(pl1);
		plist.addPlayer(pl2);
		plist.addPlayer(pl3);
		plist.addPlayer(pl4);
		plist.addPlayer(pl5);
		plist.addPlayer(pl6);
		try {
			playerListRepository.save(plist);
			System.out.println("ADD PLAYERS");

		} catch (Exception e) {
			e.printStackTrace();
		}


		
		Team team = new Team("PSY3");
		TeamMember t1 = new TeamMember("Joni", "Nordlund", "PSY", Long.valueOf(1));
		TeamMember t2 = new TeamMember("Katja", "Itkonen", "PSY", Long.valueOf(2));
		t1.setTeam(team);
		t2.setTeam(team);
		team.addPlayer(t1);
		team.addPlayer(t2);

		try {
			teamRepository.save(team);
			System.out.println("ADD TEAMS");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
