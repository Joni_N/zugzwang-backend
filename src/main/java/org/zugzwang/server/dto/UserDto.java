package org.zugzwang.server.dto;

import org.mapstruct.Mapper;
import org.zugzwang.server.model.User;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
// @NoArgsConstructor
// @Builder
public class UserDto {
    public Long id;
    public String username;
    public String email;

    public UserDto() {
    };

    public UserDto(Long id, String username, String email) {
        this.id = id;
        this.username = username;
        this.email = email;
    }

}