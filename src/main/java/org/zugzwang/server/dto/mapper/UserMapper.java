package org.zugzwang.server.dto.mapper;

import org.mapstruct.Mapper;
import org.zugzwang.server.dto.UserDto;
import org.zugzwang.server.model.User;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserDto usertoUserDto(User user);
}
