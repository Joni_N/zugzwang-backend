package org.zugzwang.server.dto.mapper;

import org.mapstruct.Mapper;
import org.zugzwang.server.dto.PlayerDto;
import org.zugzwang.server.dto.PlayerListDto;
import org.zugzwang.server.model.PlayerList;
import org.zugzwang.server.model.Player;

@Mapper(componentModel = "spring")
public interface PlayerMapper {
    PlayerDto playerToDto(Player user);
    Player dtoToPlayer(PlayerDto dto);
    PlayerListDto playerListToDto(PlayerList playerList);
}
