package org.zugzwang.server.model;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_GUEST
}
