package org.zugzwang.server.model;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name="team_member")
public class TeamMember extends Player{
 
    public TeamMember(String firstName, String lastName, String club, Long table){
        super(firstName, lastName, club);
        this.pos = table;
    }
    public TeamMember(Player player, Long table){
        super(player.getFirstName(), player.getLastName(), player.getClub());
        this.pos = table;
    }

    @ManyToOne
    @JoinColumn(name="team_id")
    private Team team;
    
    @Column(nullable = false)
    private Long pos;
}