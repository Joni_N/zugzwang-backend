package org.zugzwang.server.model;

import javax.persistence.*;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;

@Getter
@Setter
@NoArgsConstructor

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Player {
    
    public Player(String firstName, String lastName, String club) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.club = club;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 40, nullable = false)
    private String firstName;

    @Column(length = 64, nullable = false)
    private String lastName;

    @Column(length = 32, nullable = false)
    private String club;
}