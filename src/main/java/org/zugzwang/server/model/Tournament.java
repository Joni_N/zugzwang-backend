package org.zugzwang.server.model;

import javax.persistence.Entity;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class Tournament {

    enum Type {
        SINGLE, TEAM
    };
    
    public Tournament(String name, String description, LocalDate date, User user, Long numberOfTeams, Type type) {
  
        this.name = name;
        this.description = description;
        this.date = date;
        this.user = user;
        this.numberOfTeams = numberOfTeams;
        this.type = type;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 64, nullable = false)
    private String name;

    @Column(length = 512, nullable = true)
    private String description;

    @Column
    private LocalDate date;

    @Column
    private User user;

    @Column
    private Long numberOfTeams;

    @Column
    private Type type;

    @OneToMany(mappedBy = "tournament", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Team> teams = new HashSet<>();
}
