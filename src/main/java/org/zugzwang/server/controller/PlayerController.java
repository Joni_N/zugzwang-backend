package org.zugzwang.server.controller;

import java.util.Optional;

import com.google.gson.Gson;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zugzwang.server.dto.mapper.UserMapper;
import org.zugzwang.server.dto.PlayerDto;
import org.zugzwang.server.model.PlayerList;
import org.zugzwang.server.service.PlayerService;

@RestController
@RequestMapping("players")
public class PlayerController {

    @Autowired
    PlayerService playerService;

    @Autowired
    UserMapper mapper;

    @GetMapping("/list/{name}")
    ResponseEntity<ResponseWrapper> getPlayerList(@PathVariable String name) throws Exception {
        PlayerList pl = playerService.getPlayerList(name);
        return ResponseEntity.ok(ResponseWrapper.builder().status("ok")
                .message("get pl successful").result(pl).build());
    }

    @PostMapping ("/")
    public ResponseEntity<ResponseWrapper> addPlayer(@RequestBody final PlayerDto playerDto) throws Exception{
        playerService.create(playerDto);
        return ResponseEntity.ok(ResponseWrapper.builder().status("ok")
                .message("add player successful").build());
    }

    @DeleteMapping ("/")
    public ResponseEntity<ResponseWrapper> deletePlayer(@PathVariable String id) throws Exception{
        playerService.delete(id);
        return ResponseEntity.ok(ResponseWrapper.builder().status("ok")
                .message("delete player successful").build());
    }
}