package org.zugzwang.server.controller;

import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.zugzwang.server.security.jwt.JwtUtils;

@RestController
public class LoginController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/login")
    ResponseEntity<ResponseWrapper> login(@RequestBody LoginRequest loginRequest) {
        String username = loginRequest.getUsername();
        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(username,
                        loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
       
        String jwt = jwtUtils.createJwtToken(username);
        String result = new Gson().toJson(new JwtResponse(jwt));
        return ResponseEntity.ok(ResponseWrapper.builder().status("ok")
                .message("Login successful").result(result).build());
    }

    @GetMapping("/hello")
    ResponseEntity<ResponseWrapper> hello() {
        ResponseWrapper response = new ResponseWrapper("ok", "Login successful",
                "{'hello': 'hello'}");
        return ResponseEntity.ok(response);
    }
}