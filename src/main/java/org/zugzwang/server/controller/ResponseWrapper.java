package org.zugzwang.server.controller;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Builder
public class ResponseWrapper {
    private String status;
    private String message;
    private Object result;
    
    public ResponseWrapper(String status, String message, Object result) {
        this.status = status;
        this.message = message;
        this.result = result;
    }
}