package org.zugzwang.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zugzwang.server.dto.UserDto;
import org.zugzwang.server.service.UserService;

@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    UserService userService;

    // @GetMapping("/user/{id}")
    // ResponseEntity<ResponseWrapper> get(@PathVariable Long id) {
    //     return ResponseEntity.ok(ResponseWrapper.builder().status("ok")
    //             .message("Login successful").result("ere").build());
    // }

    @GetMapping("/user/{id}")
    ResponseEntity<ResponseWrapper> get(@PathVariable Long id) throws Exception {
        UserDto dto = userService.getUser(id);
    
        System.out.println(dto);    
        return ResponseEntity.ok(ResponseWrapper.builder().status("ok")
                .message("get user successful").result(dto).build());
    }
}