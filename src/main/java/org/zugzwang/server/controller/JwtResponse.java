package org.zugzwang.server.controller;

public class JwtResponse {
    private final String type = "Bearer";
    private String token;

    public JwtResponse(String token) {
        this.token = token;
    }

    public String getType() {
        return type;
    }
    
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}